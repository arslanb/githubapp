// Declare variables for pagination
var followersCurrentPage = 1;
var totalFollowers = 0;
var totalPages = 0;
var perPageRecords = 50;

// AJAX call to API for user seach
function searchGITUser(){
    $('.overlay').show();
    resetContainerSettings();
    var username = $('#githubUsername').val();
    $.ajax({
        url:    'api/user/'+username,
        method: 'GET',
        data:   null,
        success: function(res){
            var results = $.parseJSON(res);
            if(results.message == 'Not Found'){
                $('#message').text('No user found.');
                $('#userData').hide();
            } else {
                $('#message').text('User Found')
                $('#avatar').attr('src', results.avatar_url);
                $('#username').text(results.login);
                $('#usernameAPI').val(results.login);
                totalFollowers = results.followers;
                $('#followersCount').text(totalFollowers);
                totalPages = Math.ceil(totalFollowers/perPageRecords);
                $('#userData').fadeIn();
                if(totalFollowers > 0){
                    $('.collapsible').collapsible('open', 0);
                } else {
                    $('.collapsible').collapsible('close', 0);
                    $('.collapsible').collapsible('destroy');
                    $('.collapsible-header .secondary-content').hide();
                }
            }
            $('.user-data-wrapper').fadeIn();
            $('.overlay').hide();
        }
    });
}
// Form Submission event
$('#userSearchForm').on('submit', function(e){
    $('.user-data-wrapper').hide();
    searchGITUser();
    return false;
});
// Initialize Collapsible
$('.collapsible').collapsible();


// Loads the followers on call
function loadMoreFollowers(){
    $('.overlay').show();
    var username = $('#usernameAPI').val();
    $.ajax({
        url:    'api/followers/'+username+'/'+followersCurrentPage+++'/'+perPageRecords,
        method: 'GET',
        data:   null,
        success: function(res){
            var results = $.parseJSON(res);
            if(followersCurrentPage <= 2){
                $('#loadMoreFollowers').text('Load more followers');
            }
            if(followersCurrentPage >= totalPages){
                $('#loadMoreFollowers').hide();
            }
            for(i = 0;i < results.length; i++){
                var followerUiItem = $('.follower.d-none').clone();
                $(followerUiItem).children('.follower img').attr('src', results[i].avatar_url);
                $(followerUiItem).removeClass('d-none');
                $('.followers-wrapper').append(followerUiItem);
            }
            $('.overlay').hide();
        }
    });
}

// Load more followers button
$('#loadMoreFollowers').on('click', function(){
    loadMoreFollowers();
});

// Reset the UI settings and variables values
function resetContainerSettings() {
    followersCurrentPage = 1;
    totalFollowers = 0;
    totalPages = 0;
    $('.follower').not('.d-none').remove();
    $('#loadMoreFollowers').show();
    $('.collapsible-header .secondary-content').show();
}