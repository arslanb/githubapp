<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        {{-- CSS Files --}}
        <link type="text/css" rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/materialize.css') }}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('css/custom.css') }}"  media="screen,projection"/>
        {{-- End CSS Files --}}
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            {{-- User Search Form --}}
            <form action="" class="col s12 form" id="userSearchForm" method="get" >
                <div class="row">
                    <div class="col-xs-12">
                        <div class="input-field col s12">
                            <input class="validate" id="githubUsername" name="github_username" type="text" required>
                            <label for="githubUsername">Github Username</label>
                        </div>
                        <div class="input-field col s12">
                            <input id="usernameAPI" value="" type="hidden"/>
                            <button class="btn waves-effect waves-light" id="searchUser" type="submit" name="action">Submit
                                <i class="material-icons right">search</i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            {{--End User search form --}}
            {{-- User data section --}}
            <div class="row user-data-wrapper">
                <div class="col-xl-12">
                    <h2 id="message">User Found</h2>
                    <ul class="collection collapsible" id="userData" data-collapsible="accordion">
                        <li class="collection-item avatar">
                            <div class="collapsible-header">
                                <img class="circle" id="avatar" src="" alt="" >
                                <div class="col-xl-12">
                                    <ul>
                                        <li>
                                            <span class="font-weight-bold">Username:</span>
                                            <span class="title" id="username"></span>
                                        </li>
                                        <li>
                                            <span class="font-weight-bold">Followers Count:</span> <span id="followersCount"></span>
                                        </li>
                                    </ul>

                                </div>
                                <a href="#!" class="secondary-content close-accord-btn"><i class="material-icons">remove_circle</i></a>
                                <a href="#!" class="secondary-content open-accord-btn"><i class="material-icons">add_circle</i></a>
                            </div>
                            <div class="collapsible-body">
                                <div class="followers-wrapper">
                                    <div class="follower float-left d-none">
                                        <img class="custom-avator" src="https://avatars2.githubusercontent.com/u/13464393?s=84&v=4" />
                                    </div>
                                </div>
                                <button class="btn waves-effect waves-light" id="loadMoreFollowers">Load Followers</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            {{-- End user data section --}}
        </div>
        {{-- Page Overlay --}}
        <div class="overlay">
            <div class="loader">
                <div class="preloader-wrapper big active">
                    <div class="spinner-layer spinner-blue">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                    <div class="spinner-layer spinner-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                    <div class="spinner-layer spinner-yellow">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                    <div class="spinner-layer spinner-green">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- End Page Overlay --}}
        </div>
        {{-- JS Libraries --}}
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.4.1/js/tether.min.js"></script>
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/materialize.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>
        {{-- End JS Libraries --}}
    </body>
</html>
