<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// API url to user data
Route::get('/user/{name}', 'GitApiController@getUser');

// API url to get user followers
Route::get('/followers/{username}/{page}/{perPageRecords}', 'GitApiController@getUserFollowers');