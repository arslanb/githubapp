<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GitApiController extends Controller
{
	private $accessToken = 'debcfe23b123eaea833f259ec58a5c6ca0786c8c';

	/**
	 * @param $username
	 * @return string : User data in JSON
	 * @internal param : $username Get the username as parameter and search against it
	 */
	public function getUser($username){
		$ch = curl_init();
		$url = 'https://api.github.com/users/'.$username;
		$access = 'username:'.$this->accessToken;

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Agent Laravel');
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_USERPWD, $access);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch);
		$userData = json_decode(trim($output), true);
		return json_encode($userData);
	}

	/**
	 * @param $username
	 * @param int $page
	 * @param $perPageRecords
	 * @return string : Return the followers in JSON format
	 * @internal param : $username Get the username as parameter and search against it
	 * @internal param : $page counter gives the current page
	 * @internal param : $perPageRecords Pagination records
	 */
	public function getUserFollowers($username, $page = 1, $perPageRecords){
		$ch = curl_init();
		$url = 'https://api.github.com/users/'.$username.'/followers?page='.$page.'&per_page='.$perPageRecords;
		$access = 'username:'.$this->accessToken;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Agent Laravel');
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_USERPWD, $access);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$output = curl_exec($ch);
		curl_close($ch);
		$followersList = json_decode(trim($output), true);
		return json_encode($followersList);
	}
}
